﻿using UnityEngine;
using System.Collections;

public class Player1 : MonoBehaviour {

	public float speed;

	void Update () {
		Vector3 pos = this.transform.position;
		if(Input.GetKey(KeyCode.W)) {
			pos.y += speed * Time.deltaTime;
		} else if (Input.GetKey(KeyCode.S)) {
			pos.y -= speed * Time.deltaTime;
		}
		this.transform.position = pos;
	}
}
