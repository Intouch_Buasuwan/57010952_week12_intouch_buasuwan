﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIscript : MonoBehaviour {

	public int score1;
	public int score2;
	public Text P1;
	public Text P2;
	
	void Update () {
		P1.text = score1.ToString ();
		P2.text = score2.ToString ();
	}
}
