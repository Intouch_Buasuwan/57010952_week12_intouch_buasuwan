﻿using UnityEngine;
using System.Collections;

public class Player2 : MonoBehaviour {
	
	public float speed;
	
	void Update () {
		Vector3 pos = this.transform.position;
		if(Input.GetKey(KeyCode.UpArrow)) {
			pos.y += speed * Time.deltaTime;
		} else if (Input.GetKey(KeyCode.DownArrow)) {
			pos.y -= speed * Time.deltaTime;
		}
		this.transform.position = pos;
	}
}
