﻿using UnityEngine;
using System.Collections;

public class ball : MonoBehaviour {

	public Vector3 velocity;
	public GameObject UI;
	
	public void Update () {
		Vector3 pos = this.transform.position;
		pos.x += velocity.x * Time.deltaTime;
		pos.y += velocity.y * Time.deltaTime;
		this.transform.position = pos;
	}
	
	public void OnCollisionEnter (Collision other) {
		Vector3 v = new Vector3 (0, 0, 0);
		if (other.gameObject.tag == "Axis") {
			v.x = -velocity.x;
			v.y = velocity.y;
		} else if (other.gameObject.tag == "Ayis") {
			v.x = velocity.x;
			v.y = -velocity.y;
		} else if (other.gameObject.tag == "GameP1") {
			UIscript n = UI.GetComponent <UIscript> ();
			n.score2++;
			Vector3 pos = this.transform.position;
			pos.x = 0;
			pos.y = 0;
			this.transform.position = pos;
			v.x = -velocity.x;
			v.y = velocity.y;
		} else if (other.gameObject.tag == "GameP2") {
			UIscript n = UI.GetComponent <UIscript> ();
			n.score1++;
			Vector3 pos = this.transform.position;
			pos.x = 0;
			pos.y = 0;
			this.transform.position = pos;
			v.x = -velocity.x;
			v.y = velocity.y;
		}
		velocity = v;
	}
}
